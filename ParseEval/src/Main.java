import com.sun.tools.javac.util.List;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static final String STOP_LIST_PORTER_STEMMER_BM25_PATH = "sp_bm25_all_run_per_topic";
    private static final String STOP_LIST_PORTER_STEMMER_TF_IDF_PATH = "sp_tfidf_all_run_per_topic";
    private static final String NO_STOP_LIST_PORTER_STEMMER_BM25_PATH = "nsp_all_run_per_topic";
    private static final String NO_STOP_LIST_NO_PORTER_STEMMER_TF_IDF_PATH = "nsnp_all_run_per_topic";
    private static final int TOTAL_NUMBER_OF_TOPICS = 50;


    public static void main(String[] args) {
        System.out.println("Hello World!");
        try {
            System.out.println(parseFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /*
     * this class load map values from the file above, the result is a string that will be used on matlab
     * to create a matrix with all the topic per run (measure)
     */
    public static String parseFile() throws IOException {

        BufferedReader fileRead = null;
        FileReader fr = null;
        //File folder = new File("Eval");
        File[] listOfFiles = new File[4];
        listOfFiles[0] = new File(STOP_LIST_PORTER_STEMMER_BM25_PATH);
        listOfFiles[1] = new File(STOP_LIST_PORTER_STEMMER_TF_IDF_PATH);
        listOfFiles[2] = new File(NO_STOP_LIST_PORTER_STEMMER_BM25_PATH);
        listOfFiles[3] = new File(NO_STOP_LIST_NO_PORTER_STEMMER_TF_IDF_PATH);


        int run = 0;

        FileWriter w = null;
        try
        {
            w = new FileWriter("output.txt");
        }
        catch(IOException ioe)
        {
            System.out.println("write fallita.");
        }

        for (int i = 0; i < listOfFiles.length; i++)
        {
            try
            {
                String name = listOfFiles[i].getName();
                fr = new FileReader(name);
                fileRead = new BufferedReader(fr);
            }
            catch(IOException ioe)
            {
                System.out.println("eee");
            }
            System.out.println("File " + listOfFiles[i].getName());


            try
            {
                String line = fileRead.readLine();



                while (line != null)
                {

                    Scanner scanner = new Scanner(line);
                    scanner.useDelimiter(" |\t");


                    //String q0 = scanner.next();
                    String idDoc = scanner.next();

                    //String idDoc2 = scanner.next();

                    if(idDoc.equals("map"))
                    {
                        scanner.useDelimiter("\t");
                        String idDoc2 = scanner.next();
                        idDoc2 = scanner.next();
                        idDoc2 = scanner.next();
                        double s = Double.valueOf(idDoc2);
                        w.write(s+"\n");
                        System.out.println(s);
                    }

                    line = fileRead.readLine();

                }



            }
            catch(IOException ioe)
            {

            }
        }

        try
        {
            w.close();
        }
        catch(IOException ioe)
        {

        }

        //put output file into array
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("output.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String str;

        ArrayList list = new ArrayList<String>();
        if (in != null) {
            while((str = in.readLine()) != null){
                list.add(str);
            }
        }

        String[] stringArr = (String[]) list.toArray(new String[0]);

        // build final output

        String outputString = "measure=[";
        for(int i = 0;i<TOTAL_NUMBER_OF_TOPICS;i++){
            String currentString = stringArr[i] + " " + stringArr[i+TOTAL_NUMBER_OF_TOPICS] + " " + stringArr[i+(TOTAL_NUMBER_OF_TOPICS*2)] + " " + stringArr[i+(TOTAL_NUMBER_OF_TOPICS*3)] + ";";
            outputString = outputString + currentString;
        }
        outputString = outputString + "]";
        return outputString;
    }

}
