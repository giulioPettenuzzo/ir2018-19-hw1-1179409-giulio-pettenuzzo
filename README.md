# [IR2018-19-HW1] 1179409 Giulio Pettenuzzo

# CONTENUTO DIRECTORIES

# MATLAB
contiene il programma per caloclare ANOVA ONE WAY, il file con le variabili che il programma utilizza (all_measures.mat), e i plot dei risultati in formato pdf.

# ParseEval
contiene il codice java per estrarre dai file delle run i valori dell'average precision per ogni topic e preparare le variabili per il codice matlab

# no stop list no porter stemmer
contiene i file degli indici, delle valutazioni e delle run relative a TF*IDF quando l'indicizzazione è stata fatta senza l'uso delle stop-word e degli stemmer

# no stop list porter stemmer
contiene i degli indici, delle valutazioni e delle run relative a BM25 quando l'indicizzazione è stata fatta senza l'uso delle stop-word e con gli stemmer

# stop list e porter stemmer
contiene i file degli indici, delle valutazioni e delle run relative a BM25 e TF*IDF quando l'indicizzazione è stata fatta con l'uso delle stop word e con gli stemmer

# terrier-project-5.0
è la directory contenente tutto il progetto terrier, qui è possibile andare a vedere tutti i file che non stati modificati per lo sviluppo dell'homework (ad esempio terrier.properties, collection.spec ecc...)

# SECOND_RUN_TOPICS
contiene i file degli indici, delle valutazioni e delle run dove il retrival è stato fatto facendo analizzare a terrier tutto il file dei topics, (non solo il tiolo e la descrizione ma anche TOP,NUM,DESC,NARR,TITLE). Per questi risultati è stata fatta solo la valutazione per confrontare i valori statistici di base, non è stato calcolato l'ANOVA ONE WAY.

# immagini
contiene alcune immagini fondamentali come: i plot dei test statistici, gli output del codice matlab che calcola anova e un immagine con tutte le valutazioni con evidenziate le misure fondamentali.

